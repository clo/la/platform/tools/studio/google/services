/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.services;

import com.android.tools.idea.structure.services.DeveloperServiceCreator;
import com.android.tools.idea.structure.services.DeveloperServiceCreators;
import com.android.tools.idea.structure.services.ServiceContext;
import com.android.tools.idea.ui.properties.core.ObservableBool;
import com.android.tools.idea.ui.properties.core.StringValueProperty;
import com.google.common.collect.ImmutableSet;
import com.google.gct.login.GoogleLogin;
import com.google.gct.login.InvalidThreadTypeException;
import com.google.services.creators.*;

import java.util.Collection;

import static com.android.tools.idea.ui.properties.expressions.bool.BooleanExpressions.not;

/**
 * Extension for Android Studio which provides helpers that initialize Google developer services.
 */
public final class GoogleServiceCreators implements DeveloperServiceCreators {
  /**
   * Initialize this context with various variables that should be shared by all Google Services,
   * such as the user's login state and API target.
   */
  public static void initializeGoogleContext(ServiceContext serviceContext) {
    // TODO: Get this value from maven?
    serviceContext.putValue("google.play.version", new StringValueProperty("7.3.0"));
    ObservableBool loggedIn = GoogleServiceLoginListener.getInstance().loggedIn();
    serviceContext.putValue("google.isLoggedIn", loggedIn);
    serviceContext.putValue("google.isLoggedOut", not(loggedIn));

    serviceContext.putAction("google.login", new Runnable() {
      @Override
      public void run() {
        try {
          GoogleLogin.promptToLogIn();
        }
        catch (InvalidThreadTypeException e) {
          // Never happens - we always call this on the dispatch thread
          throw new RuntimeException(e);
        }
      }
    });
  }

  @Override
  public Collection<? extends DeveloperServiceCreator> getCreators() {
    return ImmutableSet.of(
      new AdMobServiceCreator(),
      new AnalyticsServiceCreator(),
      new CloudMessagingServiceCreator(),
      new IdentityServiceCreator()
    );
  }
}
